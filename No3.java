package Tugas;

import java.util.Scanner;

public class No3 {
	
	private char[][] board;
	  private int whoseturn;
	  private String[] players;
	  int movesmade;
	  final static private char[] pieces = {'X','O'};

	  public void TicTacToe(String player1, String player2) {

	    board = new char[3][3];
	    for (int indexrow=0;indexrow < 3; indexrow++)
	      for (int indexcoloumb=0; indexcoloumb < 3; indexcoloumb++)
	        board[indexrow][indexcoloumb] = '_';
	    whoseturn = 0;
	    movesmade = 0;
	    players = new String[2];
	    players[0] = player1;
	    players[1] = player2;
	  }

	  public boolean Move(int row, int column) {

	    if ( (board[row][column] == '_') && inbounds(row,column) ) {
	      board[row][column] = pieces[whoseturn];
	      movesmade++;
	      return true;
	    }
	    else
	      return false;
	  }

	  
	  public boolean inbounds(int row, int column) {

	    if ((row < 0) || (column < 0))
	      return false;
	    if ((row > 2) || (column > 2))
	      return false;
	    return true;
	  }

	  public void changeturn() {
	    whoseturn = (whoseturn + 1)%2;
	  }

	  public String getCurrentPlayer() {
	    return players[whoseturn];
	  }

	  public void printboard() {

	    System.out.println("\t0  1  2");
	    for (int i=0; i<3; i++) {
	      System.out.print(i+"\t");
	      for (int j=0; j<3; j++)
	        System.out.print(board[i][j]+"  ");
	      System.out.println();
	    }
	  }

	  
	  public char winner() {

	    for (int index=0; index<3; index++)
	      if (SameArray(board[index]) && board[index][0] != '_')
	        return board[index][0];

		
	    for (int index=0; index<3; index++)
	      if ((board[0][index] == board[1][index]) && (board[1][index] == board[2][index]) && board[0][index] != '_')
	         return board[0][index];

		
	    if ((board[0][0] == board[1][1]) && (board[1][1] == board[2][2]))
	         return board[0][0];

		
	    if ((board[2][0] == board[1][1]) && (board[1][1] == board[0][2]))
	         return board[2][0];

	    if (movesmade == 9)
	      return 'T';

	    return '_';

	  }

	  
	  private static boolean SameArray(char[] word) {

	    char check = word[0];
	    for (int index=1; index<word.length; index++)
	      if (check != word[index])
	        return false;
	    return true;
	  }

	  
	  public String whosePiece(char x) {
	    for (int index=0; index<2; index++)
	      if (x == pieces[index])
	        return players[index];
	    return "Dummy";
	  }

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
	    
	    System.out.println("Player 1, enter your name.");
	    String name1 = input.next();

	    System.out.println("Player 2, enter your name.");
	    String name2 = input.next();

	  
	    TicTacToe mygame = new TicTacToe(name1, name2);

	
	    while (mygame.winner() == '_') {

	      int player1,player2;
	      boolean done = false;

	      do {
		
	        mygame.printboard();

	        System.out.print(mygame.getCurrentPlayer());            
	        System.out.print(", Enter the row(0-2) and column(0-2) ");
		System.out.println("of your move.");

	      	player1 = input.nextInt();
	      	player2 = input.nextInt();	
	        if (!mygame.inbounds(player1,player2)) 
	          System.out.println("Sorry, those are invalid entries.");
	        else {
	          if (!mygame.Move(player1,player2))
	            System.out.println("Sorry, that square is taken.");
	          else
	            done = true;
	       }  
	      } while (!done);
		
	      mygame.changeturn();
	    }

	    mygame.printboard();
	    char win = mygame.winner();

	    if (win == 'T')
	      System.out.println("Both of you played to a tie.");
	    else {
	      System.out.print("Congratulations, " + mygame.whosePiece(win));
	      System.out.println(", you have won the game.");
	    }
		
		

	}

}
