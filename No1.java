package Tugas;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class No1 {
	
	public static void Menu() {
        System.out.println("Menu Rumus");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi Panjang");
        System.out.println("3. Segitiga Siku - Siku");
        System.out.println("4. Layang-layang");
        System.out.println("5. Belah Ketupat");
        System.out.println("6. Trapesium");
        System.out.println("7. Jajargenjang");
        System.out.println("8. Kubus");
        System.out.println("9. Balok");
        System.out.println("10.Limas");
        System.out.println("11.Prisma");
        System.out.println("12.Tabung");
        System.out.println("13.Kerucut");
        System.out.println("14.Bola");
        System.out.println("0. Keluar");
 }
 
  public static void squareFormula() {
        System.out.println("Masukan Panjang Sisi (Bilangan Desimal contohnya: 1.0): ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        try {
            inputData = bufferedReader.readLine();
        }  
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float side = Float.parseFloat(inputData);
            float wide = side * side;
            System.out.println("Luas Persegi: " + wide);
            
            float keliling = 4*side;
	         System.out.println("Keliling Persegi: " + keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void rectangleFormula() {
        System.out.println("Masukan Panjang Sisi (Bilangan Desimal contohnya: 1.0): ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Lebar Sisi : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
            inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float length = Float.parseFloat(inputDataLength);
            float width = Float.parseFloat(inputDataWidth);
            float wide = length * width;
            System.out.println("Luas Persegi Panjang: " + wide);
            
            float keliling = 2* (length + width);
            System.out.println("Keliling Persegi Panjang: " + keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void triangleFormula() {
        System.out.println("Masukan Nilai Alas Segitiga : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Nilai Tinggi Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
            inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float length = Float.parseFloat(inputDataLength);
            float width = Float.parseFloat(inputDataWidth);
            double wide = 0.5 * length * width;
            System.out.println("Luas Persegi Panjang: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
        System.out.println("Masukan sisi AB Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAB = null;
        try {
            inputDataAB = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi BC Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataBC = null;
        try {
            inputDataBC = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi AC Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAC = null;
        try {
            inputDataAC = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            float AB = Float.parseFloat(inputDataAB);
            float BC = Float.parseFloat(inputDataBC);
            float AC = Float.parseFloat(inputDataAC);
            double Keliling = AB + BC + AC;
            System.out.println("Keliling Segitiga: " + Keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  public static void TrapezoidFormula() {
        System.out.println("Masukan Jumlah sisi sejajar Trapesium : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Tinggi Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataTinggi = null;
        try {
            inputDataTinggi = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float Side = Float.parseFloat(inputDataSide);
            float Tinggi = Float.parseFloat(inputDataTinggi);
            double wide = 0.5 * Side * Tinggi;
            System.out.println("Luas Trapesium: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
        System.out.println("Masukan sisi AB Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAB = null;
        try {
            inputDataAB = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi BC Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataBC = null;
        try {
            inputDataBC = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi CD Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataCD = null;
        try {
            inputDataCD = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan sisi DA Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDA = null;
        try {
            inputDataDA = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            float AB = Float.parseFloat(inputDataAB);
            float BC = Float.parseFloat(inputDataBC);
            float CD = Float.parseFloat(inputDataCD);
            float DA = Float.parseFloat(inputDataDA);
            double Keliling = AB + BC + CD + DA;
            System.out.println("Keliling Trapesium: " + Keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void ParallelogramFormula() {
        System.out.println("Masukan Nilai Alas Jajargenjang : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAlas = null;
        try {
            inputDataAlas = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Nilai Tinggi Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataTinggi = null;
        try {
            inputDataTinggi = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float Alas = Float.parseFloat(inputDataAlas);
            float Tinggi = Float.parseFloat(inputDataTinggi);
            double wide = Alas * Tinggi;
            System.out.println("Luas Jajargenjang: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
        System.out.println("Masukan sisi AB Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAB = null;
        try {
            inputDataAB = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi BC Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataBC = null;
        try {
            inputDataBC = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi CD Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataCD = null;
        try {
            inputDataCD = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan sisi AD Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAD = null;
        try {
            inputDataAD = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            float AB = Float.parseFloat(inputDataAB);
            float BC = Float.parseFloat(inputDataBC);
            float CD = Float.parseFloat(inputDataCD);
            float AD = Float.parseFloat(inputDataAD);
            double Keliling = AB + BC + CD + AD;
            System.out.println("Keliling Jajargenjang: " + Keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void LayanglayangFormula() {
        System.out.println("Masukan Nilai Diagonal 1 Layang-layang : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal1 = null;
        try {
            inputDataDiagonal1 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Nilai Diagonal 2 Layang-layang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal2 = null;
        try {
            inputDataDiagonal2 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float Diagonal1 = Float.parseFloat(inputDataDiagonal1);
            float Diagonal2 = Float.parseFloat(inputDataDiagonal2);
            double wide = 0.5 * Diagonal1 * Diagonal2;
            System.out.println("Luas Layang-layang: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
        System.out.println("Masukan sisi AB Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataAB = null;
        try {
            inputDataAB = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan sisi BC Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataBC = null;
        try {
            inputDataBC = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        try  {
            float AB = Float.parseFloat(inputDataAB);
            float BC = Float.parseFloat(inputDataBC);
            double Keliling = 2 *(AB + BC);
            System.out.println("Keliling Layang-layang : " + Keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void RhombusFormula() {
        System.out.println("Masukan Nilai Diagonal 1 Layang-layang : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal1 = null;
        try {
            inputDataDiagonal1 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Nilai Diagonal 2 Layang-layang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal2 = null;
        try {
            inputDataDiagonal2 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float Diagonal1 = Float.parseFloat(inputDataDiagonal1);
            float Diagonal2 = Float.parseFloat(inputDataDiagonal2);
            double wide = 0.5 * Diagonal1 * Diagonal2;
            System.out.println("Luas BelahKetupat: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
        System.out.println("Masukan Panjang sisi BelahKetupat : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        try {
            inputData = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        try  {
            float Side = Float.parseFloat(inputData);
            double Keliling = 4 * Side;
            System.out.println("Keliling BelahKetupat : " + Keliling);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void CubeFormula() {
        System.out.println("Masukan Panjang Sisi : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        try {
            inputData = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float side = Float.parseFloat(inputData);
            float wide = (float) (6 * Math.pow(side,2));
            System.out.println("Luas Permukaan Kubus: " + wide);
        
	        float volume = (float) Math.pow(side,3);
	        System.out.println("Volume Kubus: " + volume);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void CuboidFormula() {
        System.out.println("Masukan Panjang Sisi : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Lebar Sisi : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
            inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan Tinggi Sisi : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHeight = null;
        try {
            inputDataHeight = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float length = Float.parseFloat(inputDataLength);
            float width = Float.parseFloat(inputDataWidth);
            float height = Float.parseFloat(inputDataHeight);
            float wide = 2*((length*width)+(length*height)+(width*height));
            System.out.println("Luas Permukaan Balok: " + wide);
        
        	float length_ = Float.parseFloat(inputDataLength);
            float width_ = Float.parseFloat(inputDataWidth);
            float height_ = Float.parseFloat(inputDataHeight);
            float wide_ = length*width*height;
	            System.out.println("Volume Balok : " + wide_);
	        }
        
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Tidak Sesuai");
	        }
  }
  public static void PyramidFormula() {
        System.out.println("Masukan Luas Alas Limas : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLuasAlas = null;
        try {
            inputDataLuasAlas = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan  jumlah luas segitiga pada bidang tegak : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDatabidangtegak = null;
        try {
            inputDatabidangtegak = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan Tinggi Limas : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHeight = null;
        try {
            inputDataHeight = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float LuasAlas = Float.parseFloat(inputDataLuasAlas);
            float bidangtegak = Float.parseFloat(inputDatabidangtegak);
            double wide = LuasAlas + bidangtegak;
            System.out.println("Luas Permukaan Limas: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
       
        try  {
            float LuasAlas = Float.parseFloat(inputDataLuasAlas);
            float Height = Float.parseFloat(inputDataHeight);
            double Volume = 0.33*LuasAlas*Height;
            System.out.println("Volume Limas: " + Volume);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
     }
  
  public static void PrismFormula() {
        System.out.println("Masukan Luas Alas Prisma : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLuasAlas = null;
        try {
            inputDataLuasAlas = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Keliling Prisma : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataKelilingPrisma = null;
        try {
            inputDataKelilingPrisma = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan Tinggi Prisma : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHeight = null;
        try {
            inputDataHeight = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float LuasAlas = Float.parseFloat(inputDataLuasAlas);
            float KelilingPrisma = Float.parseFloat(inputDataKelilingPrisma);
            float Height = Float.parseFloat(inputDataHeight);
            float wide = (2*LuasAlas)+(KelilingPrisma*Height); 
            System.out.println("Luas Permukaan Prisma: " + wide);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
        try {
        	float LuasAlas = Float.parseFloat(inputDataLuasAlas);
        	float Height = Float.parseFloat(inputDataHeight);
        	float Volume = LuasAlas*Height;
        	System.out.println("Volume Prisma: " + Volume);
	        }
	        catch(NumberFormatException e) {
	            System.out.println("Masukan Tidak Sesuai");
	        }
    }
  
  public static void CylinderFormula() {
        System.out.println("Masukan Jari-jari Tabung : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDatajarijari = null;
        try {
            inputDatajarijari = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Tinggi Tabung : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHeight = null;
        try {
            inputDataHeight = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float jarijari = Float.parseFloat(inputDatajarijari);
            float Height = Float.parseFloat(inputDataHeight);
            float widepermukaan = (float) (2*3.14*jarijari*(jarijari+Height));    
            System.out.println("Luas Permukaan Tabung: " + widepermukaan);
            
            float wideselimut = (float) (2*3.14*jarijari*Height); 
            System.out.println("Luas Selimut: " + wideselimut);
            
            float volume = (float) (3.14*Math.pow(jarijari,2)*Height); 
            System.out.println("Volume Tabung: " + volume);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void ConeFormula() {
        System.out.println("Masukan Jari-jari Kerucut : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDatajarijari = null;
        try {
            inputDatajarijari = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Panjang Garis Pelukis: ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
            inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan Tinggi Kerucut : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHeight = null;
        try {
            inputDataHeight = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float jarijari = Float.parseFloat(inputDatajarijari);
            float Length = Float.parseFloat(inputDataLength);
            float widepermukaan = (float) (3.14*jarijari*(jarijari+Length));    
            System.out.println("Luas Permukaan Kerucut: " + widepermukaan);
            
            float wideselimut = (float) (3.14*jarijari*Length); 
            System.out.println("Luas Selimut: " + wideselimut);
            
            float Height = Float.parseFloat(inputDataHeight);
            float volume = (float) (0.33*3.14*Math.pow(jarijari,2)*Height);
            System.out.println("Volume Kerucut: " + volume);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
  
  public static void SphereFormula() {
        System.out.println("Masukan Jarijari Bola : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        try {
            inputData = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            float jarijari = Float.parseFloat(inputData);
            float wide = (float) (4*3.14*Math.pow(jarijari,2));
            System.out.println("Luas Permukaan: " + wide);
            
            float Volume = (float) (1.33*3.14*Math.pow(jarijari, 3));
            System.out.println("Volume Bola: " + Volume);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai");
        }
    }
	

	public static void main(String[] args) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;
        do {
            Menu();
            System.out.println("Masukkan Pilihan Anda: ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        squareFormula();
                    }
                    else if (choice > 0 && choice == 2) {
                        rectangleFormula();
                    }
                    else if (choice > 0 && choice == 3) {
                        triangleFormula();
                    }
                    else if (choice > 0 && choice == 4) {
                    	LayanglayangFormula();
                    }
                    else if (choice > 0 && choice == 5) {
                    	RhombusFormula();
                    }
                    else if (choice > 0 && choice == 6) {
                    	TrapezoidFormula();
                    }
                    else if (choice > 0 && choice == 7) {
                    	ParallelogramFormula();
                    }
                    else if (choice > 0 && choice == 8) {
                    	CubeFormula();
                    }
                    else if (choice > 0 && choice == 9) {
                    	CuboidFormula();
                    }
                    else if (choice > 0 && choice == 10){
                    	PyramidFormula();
                    }
                    else if (choice > 0 && choice == 11){
                    	PrismFormula();
                    }
                    else if (choice > 0 && choice == 12){
                    	CylinderFormula();
                    }
                    else if (choice > 0 && choice == 13){
                    	ConeFormula();
                    }
                    else if (choice > 0 && choice == 14){
                    	SphereFormula();
                    }
                    else {
                        System.out.println("Thankyou ^_^");
                    }
                }
                catch(NumberFormatException e) {
                    System.out.println("Masukan Tidak Sesuai");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }

        } while(choice > 0);

	}

}
