package Tugas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class No2 {
	
	public static void Menu(){
		System.out.println("Masukkan Pilihan Anda");
		System.out.println("1. Perkalian Matriks");
		System.out.println("2. Penjumlahan Matriks");
		System.out.println("3. Determinan Matriks");
		System.out.println("0. Exit");
	}
	
	public static void PerkalianMatriks() {
        System.out.println("Masukan Jumlah Baris Matriks A : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataBaris = null;
        try {
            inputDataBaris = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        System.out.println("Masukan Jumlah Kolom Matriks A : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataKolom = null;
        try {
            inputDataKolom = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        

        try  {
        	Scanner input = new Scanner(System.in);
            int Baris = Integer.parseInt(inputDataBaris);
            int Kolom = Integer.parseInt(inputDataKolom);
           
            		int[][]matriksA = new int [Baris][Kolom];
    		
    		System.out.println("Matriks A :");
    			for (int indeksbaris=0; indeksbaris<matriksA.length; indeksbaris++){
    				for (int indekskolom=0; indekskolom<matriksA[indeksbaris].length; indekskolom++){
    					System.out.print("Baris ["+(indeksbaris+1)+"] Kolom ["+(indekskolom+1)+"] :");
    					matriksA[indeksbaris][indekskolom]=input.nextInt();
    				}
    				
    			}
    			System.out.println();
    			System.out.println("Matriks A : "+Baris+" x " +Kolom);
    				for (int indeksbaris=0; indeksbaris<Baris; indeksbaris++){
    					for(int indekskolom=0; indekskolom<Kolom; indekskolom++){
    						System.out.print(matriksA[indeksbaris][indekskolom]+ " ");
    					}
    					System.out.println();
    				}
    				System.out.println();
    				
    		
    		System.out.println("");
    		System.out.println("Masukkan Jumlah Baris Matriks B :");
    		int Baris_ = input.nextInt();
    		System.out.println("Masukkan Jumlah Kolom Matriks B  :");
    		int Kolom_ = input.nextInt();

    		int[][]matriksB = new int [Baris][Kolom_];
    			
    		System.out.println("Matriks B :");
    			for(int indeksbaris=0; indeksbaris<matriksB.length; indeksbaris++){
    				for (int indekskolom=0; indekskolom<matriksB[indeksbaris].length; indekskolom++){
    					System.out.print("Baris ["+(indeksbaris+1)+"] Kolom ["+(indekskolom+1)+"] :");
    					matriksB[indeksbaris][indekskolom]=input.nextInt();
    				}
    			}

    		
    		System.out.println();
    			System.out.println("Matriks B : "+Kolom+" x " +Kolom_);
    				for (int indeksbaris=0; indeksbaris<Kolom; indeksbaris++){
    					for(int indekskolom=0; indekskolom<Kolom_; indekskolom++){
    						System.out.print(matriksB[indeksbaris][indekskolom]+ " ");
    					}
    					System.out.println();
    				}
    				System.out.println();
    		
    				
    				
    				
    		int[][]matriksC = new int [Baris][Kolom_];
    		System.out.println("Hasil dari perkalian matriks A x B: ");
    			for(int indeksbaris=0; indeksbaris<Baris; indeksbaris++){
    				for(int indekskolom=0; indekskolom<Kolom_; indekskolom++){
    					for (int indeks=0; indeks<Kolom; indeks++){
    						matriksC[indeksbaris][indekskolom] +=matriksA[indeksbaris][indeks]*matriksB[indeks][indekskolom];
    					}
    					System.out.print(matriksC[indeksbaris][indekskolom]+" ");
    				}
    				System.out.println();
    			}
            		
            		
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai (Coba Lagi!)");
        }
    }
	
	public static void DeterminanMatriks() {
        System.out.println("Masukan Ordo Matriks : ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataordo = null;
        try {
            inputDataordo = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
        	Scanner input = new Scanner(System.in);
            int  ordo = Integer.parseInt(inputDataordo);
            
    		int[][]matriks = new int [ordo][ordo];
    		
    		if(ordo==2){
    		
    			for (int indekbaris=0;indekbaris<ordo;indekbaris++){
    				for (int indekskolom=0;indekskolom<ordo;indekskolom++){
    					System.out.print("Baris ["+(indekbaris+1)+"] Kolom ["+(indekskolom+1)+"] : ");
    					matriks[indekbaris][indekskolom]=input.nextInt();
    					
    				}
    			}
    			
    			System.out.println();
    			System.out.println("Matriks : "+ordo+" X "+ordo);
    			for (int indeksbaris=0;indeksbaris<ordo;indeksbaris++){
    				for (int indekskolom=0;indekskolom<ordo;indekskolom++){
    					System.out.printf("%5d",matriks[indeksbaris][indekskolom]);
    					
    				}
    				System.out.println();
    				
    				
    			}
    			System.out.println();

    					int determinan = (matriks[0][0]*matriks[1][1])-(matriks[0][1]*matriks[1][0]);
    					
    					
    			System.out.println("Determinannya adalah : "+determinan);

    			
    		}
    		
    		else if (ordo==3){
    		for (int indeksbaris=0;indeksbaris<ordo;indeksbaris++){
    			for (int indekskolom=0;indekskolom<ordo;indekskolom++){
    				System.out.print("Baris ["+(indeksbaris+1)+"] Kolom ["+(indekskolom+1)+"] : ");
    				matriks[indeksbaris][indekskolom]=input.nextInt();
    				
    			}
    		}
    		
    		System.out.println();
    		System.out.println("Matriks : "+ordo+" X "+ordo);
    		for (int indeksbaris=0;indeksbaris<ordo;indeksbaris++){
    			for (int indekskolom=0;indekskolom<ordo;indekskolom++){
    				System.out.printf("%5d",matriks[indeksbaris][indekskolom]);
    				
    			}
    			System.out.println();
    			
    			
    		}
    		System.out.println();

    				int determinan = matriks[0][0]*(matriks[1][1]*matriks[2][2]-matriks[1][2]*matriks[2][1])
    								-matriks[0][1]*(matriks[1][0]*matriks[2][2]-matriks[1][2]*matriks[2][0])
    								+matriks[0][2]*(matriks[1][0]*matriks[2][1]-matriks[1][1]*matriks[2][0]);
    				
    				
    		System.out.println("Determinannya adalah : "+determinan);
    		
    	}
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai (Coba Lagi!)");
        }
    }
	
	public static void PenjumlahanMatriks() {
        System.out.println("Masukan Jumlah Baris: ");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRow = null;
        try {
            inputDataRow = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.println("Masukan Jumlah Kolom: ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataColoumb = null;
        try {
            inputDataColoumb = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
        	
        	Scanner input = new Scanner (System.in);
        	 int baris = Integer.parseInt(inputDataRow);
        	 int kolom = Integer.parseInt(inputDataColoumb);
          
            System.out.println("Masukan Data : ");
	        System.out.println("Matriks A : ");
	        int matriksA[][] = new int[baris][kolom];
	        for (int indeksbaris = 0; indeksbaris < baris; indeksbaris++) {
	            for (int indekskolom = 0; indekskolom < kolom; indekskolom++) {
	            	System.out.print("Baris ["+(indeksbaris+1)+"] Kolom ["+(indekskolom+1)+"] :");
	                matriksA[indeksbaris][indekskolom] = input.nextInt();
	            }
	        }
	        System.out.println("Matriks B : ");
	        int matriksB[][] = new int[baris][kolom];
	        for (int indeksbaris = 0; indeksbaris < baris; indeksbaris++) {
	            for (int indekskolom = 0; indekskolom < kolom; indekskolom++) {
	            	System.out.print("Baris ["+(indeksbaris+1)+"] Kolom ["+(indekskolom+1)+"] :");
	                matriksB[indeksbaris][indekskolom] = input.nextInt();
	            }
	        }
	        System.out.println("Matriks A");
	        for (int indeksbaris = 0; indeksbaris < baris; indeksbaris++) {
	            for (int indekskolom = 0; indekskolom < kolom; indekskolom++) {
	                System.out.print(matriksA[indeksbaris][indekskolom]);
	                System.out.print(" ");
	            }
	            System.out.println(" ");
	        }

	        System.out.println("Matriks B");
	        for (int indeksbaris = 0; indeksbaris < baris; indeksbaris++) {
	            for (int indekskolom = 0; indekskolom < kolom; indekskolom++) {
	                System.out.print(matriksB[indeksbaris][indekskolom]);
	                System.out.print(" ");
	            }
	            System.out.println(" ");
	        }
	        System.out.println("Hasil Matriks");
	        for (int indeksbaris = 0; indeksbaris < baris; indeksbaris++) {
	            for (int indekskolom = 0; indekskolom < kolom; indekskolom++) {
	                System.out.print(matriksA[indeksbaris][indekskolom] + matriksB[indeksbaris][indekskolom]);
	                System.out.print(" ");
	            }
	            System.out.println(" ");

	        }
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Tidak Sesuai (Coba Lagi!)");
        }
    }


	public static void main(String[] args) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;
        do {
            Menu();
            System.out.println("Masukkan Pilihan Anda: ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        PerkalianMatriks();
                    }
                    else if (choice > 0 && choice == 2) {
                        PenjumlahanMatriks();
                    }
                    else if (choice > 0 && choice == 3) {
                        DeterminanMatriks();
                    }
                    else {
                        System.out.println("Thankyou ^_^");
                    }
                }
                catch(NumberFormatException e) {
                    System.out.println("Masukan Tidak Sesuai");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }
            System.out.println(" ");

        } while(choice > 0);
		

		
	}

}
